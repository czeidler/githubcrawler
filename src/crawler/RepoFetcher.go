package main
import (
	"os"
	"path"
	"sync"
	"strings"
)

type FetchedRepo struct {
	repoItem RepoItem
	dir string
}

type RepoFetcher struct {
	baseDir string
	pullExisting bool
	channelSize int
}

func (this *RepoFetcher) goFetch(in <-chan RepoItem) chan FetchedRepo {
	var out = make(chan FetchedRepo, this.channelSize)
	go this.fetch(in, out)
	return out
}

func (this *RepoFetcher) fetch(in <-chan RepoItem, out chan<- FetchedRepo) {
	defer close(out)
	var waitGroup sync.WaitGroup
	nWorker := make(chan int, this.channelSize)
	for repoItem := range in {
		waitGroup.Add(1);
		nWorker <- 1
		go func(item RepoItem) {
			defer waitGroup.Done()
			this.fetchRepo(item, out)
			<- nWorker
		}(repoItem)
	}
	waitGroup.Wait()
	println("fetch repos done")
}


func (this *RepoFetcher) fetchRepo(item RepoItem, out chan <- FetchedRepo) {
	var repo = FetchedRepo{item, ""}

	repo.dir = path.Join(this.baseDir, "repos", item.Name)
	os.MkdirAll(repo.dir, 0770)

	gitDir := path.Join(repo.dir, ".git");

	var gitCommand string
	url := item.Html_url
	replacer := strings.NewReplacer("https://", "https://:@")
	url = replacer.Replace(url)

	if _, err := os.Stat(gitDir); os.IsNotExist(err) {
		println("Clone: " + url + " to " + repo.dir)
		gitCommand = "git clone --depth=1 " + url + " " + repo.dir
	} else if this.pullExisting {
		println("Update: " + url)
		gitCommand = "cd " + repo.dir + " && git pull --depth=1"
	} else {
		out <- repo
		return
	}

	stdout, error := bashCommandErr(gitCommand)
	if error != nil {
		println("fetchRepo error: " + gitCommand + " " + string(stdout))
		return
	}

	println(string(stdout))
	out <- repo
}