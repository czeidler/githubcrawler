package main
import (
	"sort"
	"strconv"
)

type ElementStats struct {
	element *XMLElement
	elementType string
	isLayout bool
	id string
	parentChainHash string
	parentSiblingHash string
	childHash string
}

type ElementIdStatsMap struct {
	elementMap map[string]*ElementStats
}

type ElementIdComparison struct {
	nElements1                int
	nElements2                int
	nMatches                  int
	nMatchedParentChainHash   int
	nMatchedParentSiblingHash int
	nMatchedChildHash         int
	nTypeChanges              int
}

func elementIdComparisonCSHeaderString(prefix string) string {
	return prefix + "nElements1," +
		prefix + "nElements2," +
		prefix + "nMatches," +
		prefix + "nMatchedParentChainHash," +
		prefix + "nMatchedParentSiblingHash," +
		prefix + "nMatchedChildHash," +
		prefix + "nTypeChanges"
}

func (this* ElementIdComparison)toCSString() string {
	return strconv.Itoa(this.nElements1) + "," +
		strconv.Itoa(this.nElements2) + "," +
		strconv.Itoa(this.nMatches) + "," +
		strconv.Itoa(this.nMatchedParentChainHash) + "," +
		strconv.Itoa(this.nMatchedParentSiblingHash) + "," +
		strconv.Itoa(this.nMatchedChildHash) + "," +
		strconv.Itoa(this.nTypeChanges);
}

func compare(map1 *ElementIdStatsMap, map2 *ElementIdStatsMap) *ElementIdComparison {
	comparison := ElementIdComparison{}
	comparison.nElements1 = len(map1.elementMap)
	comparison.nElements2 = len(map2.elementMap)

	for id, elementStat := range map1.elementMap {
		elementStat2, ok := map2.elementMap[id]
		if !ok {
			continue
		}
		comparison.nMatches++
		if elementStat.parentChainHash == elementStat2.parentChainHash {
			comparison.nMatchedParentChainHash++
		}
		if elementStat.childHash == elementStat2.childHash {
			comparison.nMatchedChildHash++
		}
		if elementStat.parentSiblingHash == elementStat2.parentSiblingHash {
			comparison.nMatchedParentSiblingHash++
		}
		if elementStat.elementType != elementStat2.elementType {
			comparison.nTypeChanges++;
		}
	}
	return &comparison
}

func compareLayouts(root1 *XMLElement, root2 *XMLElement) (layout *ElementIdComparison, elements *ElementIdComparison){
	layoutMap1 := NewElementIdStatsMap()
	elementMap1 := NewElementIdStatsMap()
	root1.iterate(func(element *XMLElement) {
		elementStats := getElementStats(element)
		layoutMap1.addIfLayout(elementStats)
		elementMap1.addIfElement(elementStats)
	})
	layoutMap2 := NewElementIdStatsMap()
	elementMap2 := NewElementIdStatsMap()
	root2.iterate(func(element *XMLElement) {
		elementStats := getElementStats(element)
		layoutMap2.addIfLayout(elementStats)
		elementMap2.addIfElement(elementStats)
	})
	layoutComparison := compare(layoutMap1, layoutMap2)
	elementComparison := compare(elementMap1, elementMap2)
	return layoutComparison, elementComparison
}

func (this* XMLElement)iterate(call func(element *XMLElement)) {
	call(this)
	for _, child := range this.child {
		child.iterate(call)
	}
}

func NewElementIdStatsMap() *ElementIdStatsMap {
	statsMap := ElementIdStatsMap{}
	statsMap.elementMap = make(map[string]*ElementStats, 0)
	return &statsMap
}

func (this *ElementIdStatsMap)addIfElement(elementStats *ElementStats) {
	if elementStats.id == "" {
		return
	}
	if elementStats.isLayout {
		return
	}
	this.elementMap[elementStats.id] = elementStats
}

func (this *ElementIdStatsMap)addIfLayout(elementStats *ElementStats) {
	if elementStats.id == "" {
		return
	}
	if !elementStats.isLayout {
		return
	}
	this.elementMap[elementStats.id] = elementStats
}

func getElementStats(element *XMLElement) *ElementStats {
	stats := ElementStats{}
	stats.element = element
	stats.elementType = getViewType(element)
	stats.isLayout = isStandardLayout(stats.elementType)
	stats.id = getElementId(element)
	stats.parentChainHash = parentChainHash(element)
	stats.parentSiblingHash = parentSiblingHash(element)
	stats.childHash = childHash(element, false)
	return &stats
}

func getElementId(element *XMLElement) string  {
	for _, attr := range element.attrs {
		// add android:id if exist
		if attr.Name.Local == "id" {
			return attr.Value
		}
	}
	return ""
}

func parentChainHash(element *XMLElement) string {
	hashData := ""
	for parent := element.parent; parent != nil; parent = parent.parent {
		hashData += getViewType(parent)
		hashData += getElementId(parent)
	}
	return hexHash(hashData)
}

func viewSiblingHash(element *XMLElement) string {
	if element.parent == nil {
		return hexHash("")
	}
	return childHash(element.parent, true)
}

func parentSiblingHash(element *XMLElement) string {
	return hexHash(parentChainHash(element) + viewSiblingHash(element));
}

func childHash(element *XMLElement, includeElementsWithoutId bool) string {
	hashData := ""
	var siblingHashes []string
	for _, sibling := range element.child {
		if sibling == element {
			continue
		}
		siblingId := getElementId(sibling)
		if siblingId == "" && !includeElementsWithoutId {
			continue
		}

		siblingHash := getViewType(sibling) + siblingId
		siblingHashes = append(siblingHashes, hexHash(siblingHash))
	}
	sort.Strings(siblingHashes)
	for _, siblingHash := range siblingHashes {
		hashData += siblingHash
	}
	return hexHash(hashData)
}
