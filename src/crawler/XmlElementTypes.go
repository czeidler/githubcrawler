package main
import "strings"

type XMLElementTypes struct {
	rootLayoutCounts      map[string]int
	layoutCounts      map[string]int
	elementCounts     map[string]int

	totalElementCount int
	// element name -> layout file
	unknownElements   map[string]string

	elementsHash      map[*XMLElement]string
}

var layoutTypes = []string{
	"AbsoluteLayout",
	"FrameLayout",
	"LinearLayout",
	"LinearLayout_horizontal",
	"LinearLayout_vertical",
	"TableLayout",
	"GridLayout",
	"RelativeLayout"}


var elementTypes = []string{
	"AnalogClock",
	"AutoCompleteTextView",
	"Button",
	"CheckBox",
	"CheckedTextView",
	"Chronometer",
	"DatePicker",
	"DigitalClock",
	"EditText",
	"ExpandableListView",
	"fragment",
	"GridView",
	"HorizontalScrollView",
	"IconTextView",
	"ImageView",
	"ImageButton",
	"ListView",
	"NumberPicker",
	"RadioButton",
	"RadioGroup",
	"RatingBar",
	"ProgressBar",
	"ScrollView",
	"SeekBar",
	"Space",
	"Spinner",
	"StackView",
	"SurfaceView",
	"Switch",
	"TabHost",
	"TableRow",
	"TabWidget",
	"TextSwitcher",
	"TextView",
	"ToggleButton",
	"TimePicker",
	"TwoLineListItem",
	"VideoView",
	"ViewAnimator",
	"ViewFlipper",
	"ViewPager",
	"ViewStub",
	"ViewSwitcher",
	"WebView",
	"android.support.v4.widget.DrawerLayout",
	"android.support.v4.view.ViewPager",
	"android.support.v7.widget.CardView",
	"android.support.v7.widget.RecyclerView",
	"android.support.v7.widget.Toolbar",
	"custom",
	"view_spacer"}

func NewXMLElementTypes() XMLElementTypes {
	this := XMLElementTypes{}
	this.rootLayoutCounts = make(map[string]int)
	this.layoutCounts = make(map[string]int)
	this.elementCounts = make(map[string]int)
	this.elementsHash = make(map[*XMLElement]string)
	this.unknownElements = make(map[string]string)
	return this
}

func contains(array []string, searchItem string) bool {
	for _, item := range array {
		if (item == searchItem) {
			return true
		}
	}
	return false
}

func isStandardLayout(name string) bool {
	return contains(layoutTypes, name)
}

func isStandardElement(name string) bool {
	return contains(elementTypes, name)
}

func getViewType(element *XMLElement) string {
	elementName := element.name.Local
	if strings.ToUpper(elementName) != strings.ToUpper("view") {
		return elementName
	}
	for _, attr := range element.attrs {
		if attr.Name.Local == "class" {
			return attr.Value
		}
	}
	return "view_spacer"
}

func (this *XMLElementTypes)analyse(element *XMLElement, layoutFile string) {
	elementName := getViewType(element)
	if isStandardLayout(elementName) {
		this.layoutCounts[elementName] ++
		if element.parent == nil {
			this.rootLayoutCounts[elementName] ++
		}
		if elementName == "LinearLayout" {
			orientationName := elementName + "_" + element.getLinearLayoutOrientation()
			this.layoutCounts[orientationName] ++
			if element.parent == nil {
				this.rootLayoutCounts[orientationName] ++
			}
		}
	} else if isStandardElement(elementName) {
		this.elementCounts[elementName] = this.elementCounts[elementName] + 1
		this.totalElementCount++
		this.elementsHash[element] = element.hash()
	} else {
		this.elementCounts["custom"] ++
		this.totalElementCount++
		this.unknownElements[elementName] = layoutFile
	}

	for _, child := range element.child {
		// TODO don't follow if it is a scrollview?
		this.analyse(child, layoutFile)
	}
}