package main

import "encoding/json"
import (
	"net/http"
	"bytes"
	"strconv"
	"fmt"
	"os"
	"path"
	"io/ioutil"
	"errors"
	"time"
)


type RepoItem struct {
	Name string
	Description string
	Html_url string
}

type GitHubSearchResult struct {
	Items[] RepoItem
}

type UrlFetcher struct {
	outDir string
	lastFetchTime time.Time
	nItems int
	itemsPerPage int
	useCachedUrls bool
	searchTerm string
	channelSize int
}

func (this *UrlFetcher) goFetch() chan RepoItem {
	var urlFetchedChannel = make(chan RepoItem, this.channelSize)
	go this.fetch(urlFetchedChannel)
	return urlFetchedChannel
}

func getCacheFile(page int, itemsPerPage int) string {
	return "page_" + strconv.Itoa(page) + "_" + strconv.Itoa(itemsPerPage)
}

func readCachedPage(cacheDir string, page int, itemsPerPage int) ([]byte, error) {
	var buffer []byte

	cacheFile := path.Join(cacheDir, getCacheFile(page, itemsPerPage))
	if _, err := os.Stat(cacheFile); os.IsNotExist(err) {
		return buffer, errors.New("Not cached")
	}

	buffer, err := ioutil.ReadFile(cacheFile)
	if err != nil {
		return buffer, nil
	}

	return buffer, nil
}

func (this *UrlFetcher) downloadPage(cacheDir string, page int, itemsPerPage int) ([]byte, error) {
	// don't ask to often
	if time.Since(this.lastFetchTime) < 2 {
		time.Sleep(2)
	}

	language := "java"

	response, err := http.Get("https://api.github.com/search/repositories?q=" + this.searchTerm + "+language:" +
	language + "&sort=stars&order=desc&page=" + strconv.Itoa(page) + "&per_page=" + strconv.Itoa(itemsPerPage));
	if err != nil {
		return nil, err
	}

	this.lastFetchTime = time.Now()

	jsonBuf := new(bytes.Buffer)
	jsonBuf.ReadFrom(response.Body)

	// cache
	os.MkdirAll(cacheDir, 0770)
	cacheFile := path.Join(cacheDir, getCacheFile(page, itemsPerPage))
	ioutil.WriteFile(cacheFile, jsonBuf.Bytes(), 0770)

	return jsonBuf.Bytes(), nil
}

func (this *UrlFetcher) getPage(cacheDir string, page int, itemsPerPage int) (GitHubSearchResult, error) {
	var result GitHubSearchResult

	var buffer []byte
	var err error = errors.New("Unset")
	if this.useCachedUrls {
		buffer, err = readCachedPage(cacheDir, page, itemsPerPage)
	}
	if err != nil {
		buffer, err = this.downloadPage(cacheDir, page, itemsPerPage)
	}
	if err != nil {
		println(err.Error())
		return result, err
	}

	err = json.Unmarshal(buffer, &result)

	return result, err
}

func (this *UrlFetcher) fetch(out chan<- RepoItem) {
	defer close(out)

	page := 0
	for item := 0; item < this.nItems; item++ {
		nextPage := item / this.itemsPerPage + 1
		if page == nextPage {
			break
		}
		page = nextPage

		result, err := this.getPage(path.Join(this.outDir, "cache"), page, this.itemsPerPage)
		if err != nil {
			println("getPage: " + err.Error())
			return
		}
		if len(result.Items) == 0 {
			fmt.Printf("Bad result: %+v\n", result)
		}

		for i := 0; i < len(result.Items); i++ {
			if item + i >= this.nItems {
				break;
			}
			repoItem := result.Items[i]
			if repoItem.Html_url != "" {
				out <- repoItem;
			}
		}
		item += len(result.Items)
	}
}