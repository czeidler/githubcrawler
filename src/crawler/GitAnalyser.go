package main

import (
	"sync"
	"strings"
	"io"
	"bufio"
	"bytes"
	"sort"
	"io/ioutil"
	"path"
	"os"
	"container/list"
	"encoding/hex"
	"crypto/sha1"
	"strconv"
)


type AnalysisResult struct {
	repo FetchedRepo
	libInDescription bool
	isApp bool

	mainLayoutDirsXML []string
	mainLayoutElementTypes XMLElementTypes
	mainLayoutCount int

	inCodeLayoutAnalysis InCodeLayoutAnalysis

	multiLayoutXML MultiLayoutXML
}

func analysisResultHeader(types XMLElementTypes) string {
	var header string
	header += "name,"
	header += "isApp,"
	header += "libInDescription,"
	header += writeHeaderXMLTypes(types)
	header += "nMainLayoutDirsXML,"
	header += "nMainLayouts,"
	header += "nMultiXML,"
	header += "nDifferentMultiXML,"
	header += inCodeLayoutAnalysisHeader("nNew_") + ","
	return header
}

func (this *AnalysisResult) toCCString() string {
	var out string
	out += this.repo.repoItem.Name + ","
	out += btoa(this.isApp) + ","
	out += btoa(this.libInDescription) + ","
	out += writeXMLTypes(this.mainLayoutElementTypes)
	out += strconv.Itoa(len(this.mainLayoutDirsXML)) + ","
	out += strconv.Itoa(this.mainLayoutCount) + ","
	out += strconv.Itoa(len(this.multiLayoutXML.matches)) + ","
	out += strconv.Itoa(this.multiLayoutXML.countDifferentHashMatches()) + ","
	out += this.inCodeLayoutAnalysis.toCSString() + ","
	return out
}


func NewAnalysisResult(repo FetchedRepo) AnalysisResult {
	this := AnalysisResult{repo: repo}
	this.mainLayoutElementTypes = NewXMLElementTypes()
	this.inCodeLayoutAnalysis = InCodeLayoutAnalysis{}
	return this
}

type MultiLayoutMatchXML struct {
	baseDir             string
	xmlFile             string

	layoutDir0          string
	hash0               string
	elementTypes0       *XMLElementTypes

	layoutDir1          string
	hash1               string
	elementTypes1       *XMLElementTypes

	layoutIdComparison  *ElementIdComparison
	elementIdComparison *ElementIdComparison

	// comparison between both layouts
	nSameElementHash    int
}

func multiLayoutMatchXMLCSHeaderString() string {
	var out string
	out += "xmlFile,"
	out += "layoutDir0,"
	out += "totalElementCount0,"
	out += "hash0,"
	out += "layoutDir1,"
	out += "totalElementCount1,"
	out += "hash1,"
	out += "nSameElementHash,"
	out += elementIdComparisonCSHeaderString("layout_") + ","
	out += elementIdComparisonCSHeaderString("element_") + ","
	return out
}

func (this *MultiLayoutMatchXML)toCSString() string {
	var out string
	out += path.Join(this.baseDir, this.layoutDir1, this.xmlFile) + ","
	out += this.layoutDir0 + ","
	out += strconv.Itoa(this.elementTypes0.totalElementCount) + ","
	out += this.hash0 + ","
	out += this.layoutDir1 + ","
	out += strconv.Itoa(this.elementTypes1.totalElementCount) + ","
	out += this.hash1 + ","
	out += strconv.Itoa(this.nSameElementHash) + ","
	out += this.layoutIdComparison.toCSString() + ","
	out += this.elementIdComparison.toCSString() + ","
	return out
}

func NewMultiLayoutMatchXML(baseDir string, xmlFile string, layoutDir0 string,
	element0 *XMLElement, elementTypes0 *XMLElementTypes, layoutDir1 string, element1 *XMLElement,
	elementTypes1 *XMLElementTypes) (this *MultiLayoutMatchXML) {

	this = &MultiLayoutMatchXML{}
	this.baseDir = baseDir
	this.xmlFile = xmlFile

	this.layoutDir0 = layoutDir0
	this.hash0 = element0.hash()
	this.elementTypes0 = elementTypes0

	this.layoutDir1 = layoutDir1
	this.hash1 = element1.hash()
	this.elementTypes1 = elementTypes1

	this.layoutIdComparison, this.elementIdComparison = compareLayouts(element0, element1)

	for _, hash0 := range this.elementTypes0.elementsHash {
		for _, hash1 := range this.elementTypes1.elementsHash {
			if hash0 == hash1 {
				this.nSameElementHash ++
				break
			}
		}
	}
	return this
}

type MultiLayoutXML struct {
	matches []*MultiLayoutMatchXML
}

func (this *MultiLayoutXML)countDifferentHashMatches() (n int) {
	for _, match := range this.matches {
		if match.hash0 != match.hash1 {
			n++
		}
	}
	return n
}

type RepoAnalyser struct {
	channelSize int
}

func (this *RepoAnalyser) goAnalyse(in <-chan FetchedRepo) chan AnalysisResult {
	var out = make(chan AnalysisResult, this.channelSize)
	go this.analyseRepos(in, out)
	return out
}

func (this *RepoAnalyser) analyseRepos(in <-chan FetchedRepo, out chan<- AnalysisResult) {
	defer close(out)
	var waitGroup sync.WaitGroup
	nWorker := make(chan int, this.channelSize)
	for repoItem := range in {
		waitGroup.Add(1);
		nWorker <- 1
		go func(item FetchedRepo) {
			defer waitGroup.Done()
			this.analyseRepo(item, out)
			<- nWorker
		}(repoItem)
	}
	waitGroup.Wait()
	println("analyse repos done")
}

func isApp(repo *FetchedRepo) bool {
	out := bashCommandArray("grep -r \"<application\" --include=AndroidManifest.xml " + repo.dir)
	return len(out) > 0
}

func toLineArray(in io.Reader) (lines []string) {
	scanner := bufio.NewScanner(in)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return
}

func bashCommandArray(command string) []string {
	return toLineArray(bytes.NewReader(bashCommand(command)))
}

func getMainLayoutDirs(dir string) []string {
	return bashCommandArray("find " + dir + " -path \"*/res/layout\" -not -path \"*/build/*\"")
}

func getAllLayoutDirs(dir string) []string {
	return bashCommandArray("find " + dir + " -regex \".*/res/layout[a-zA-Z0-9\\-]*$\" -not -path \"*/build/*\"")
}

func getLayoutTags(layoutDir string, layout string) (lines []string) {
	lines = bashCommandArray("grep -r \"<" + layout +"\" " + layoutDir + "/*")
	return lines
}

func listXMLFiles(dir string) (xmlFiles []string) {
	files, _ := ioutil.ReadDir(dir)
	for _, f := range files {
		if !strings.HasSuffix(f.Name(), ".xml") {
			continue
		}
		xmlFiles = append(xmlFiles, f.Name())
	}
	return xmlFiles
}

// follow includes
func parseExpandIncludes(baseDir string, xmlFile string) *XMLElement {
	parsedFiles := list.New()
	return parseExpandIncludesIntern(baseDir, xmlFile, parsedFiles)
}

func parseExpandIncludesIntern(baseDir string, xmlFile string, parsedFiles *list.List) *XMLElement {
	for e := parsedFiles.Front(); e != nil; e = e.Next() {
		if (e.Value.(string) == xmlFile) {
			return nil
		}
	}
	parsedFiles.PushBack(xmlFile)

	root := parseXMLFile(path.Join(baseDir, xmlFile))
	if (root == nil) {
		return root
	}
	allElements := collectXMLElements(root)
	var element *XMLElement
	for ; len(allElements) > 0; {
		element = allElements[0]
		allElements = allElements[1:]
		if (element.name.Local == "include") {
			var layout string
			for _, attr:= range element.attrs {
				if attr.Name.Local == "layout" {
					layout = attr.Value
					replacer := strings.NewReplacer("@", "")
					layout = replacer.Replace(layout)
					break;
				}
			}
			if layout == "" {
				continue
			}
			childLayout := parseExpandIncludesIntern(baseDir, "../" + layout + ".xml", parsedFiles)
			if (childLayout == nil) {
				continue
			}
			childLayout.parent = element.parent
			allElements = append(allElements, collectXMLElements(childLayout)...)
			// replace the include element with the parsed
			for i, child:= range element.parent.child {
				if (child == element) {
					element.parent.child = append(element.parent.child[:i],
						append([]*XMLElement{childLayout}, element.parent.child[i + 1:]...)...)
					break
				}
			}
		}
	}

	return root
}

func (this *XMLElement)getLinearLayoutOrientation() string {
	linearLayoutOrientation := "horizontal"
	for _, attr := range this.attrs {
		// add orientation for LinearLayouts
		if attr.Name.Local == "orientation" {
			linearLayoutOrientation = strings.ToLower(attr.Value)
			break
		}
	}
	return linearLayoutOrientation
}

func (this *XMLElement)shallowHash() string {
	hashData := this.name.Local
	for _, attr := range this.attrs {
		// add android:id if exist
		if attr.Name.Local == "id" {
			hashData += attr.Name.Local
			hashData += attr.Value
		}
	}
	if this.name.Local == "LinearLayout" {
		hashData += "orientation:" + this.getLinearLayoutOrientation()
	}
	return hashData
}

func (this *XMLElement)forwardHash() string {
	hashData := this.shallowHash()

	var childHashes []string
	for _, child := range this.child {
		childHashes = append(childHashes, child.forwardHash())
	}
	sort.Strings(childHashes)
	for _, childHash := range childHashes {
		hashData += childHash
	}
	hash := sha1.Sum([]byte(hashData))
	return hex.EncodeToString(hash[:])
}

func hexHash(data string) string {
	hash := sha1.Sum([]byte(data))
	return hex.EncodeToString(hash[:])
}

func (this *XMLElement)hash() string {
	hashData := this.forwardHash()

	// include parent shallow hashes
	for parent := this.parent; parent != nil; parent = parent.parent {
		hashData += parent.shallowHash()
	}

	return hexHash(hashData)
}

func parseXMLFile(xmlFile string) *XMLElement {
	var xmlElement = XMLElement{}
	f, err := os.Open(xmlFile)
	defer f.Close()
	if err != nil {
		return nil
	}
	xmlElement.parse(bufio.NewReader(f))
	return &xmlElement
}

func collectXMLElements(root *XMLElement) ([]*XMLElement) {
	elements := make([]*XMLElement, 0)
	for _,child := range root.child {
		elements = append(elements, child)
		childElements := collectXMLElements(child);
		elements = append(elements, childElements...)
	}
	return elements
}

func countXMLElements(root *XMLElement, elementName string) (count int) {
	if (root.name.Local == elementName) {
		count++
	}
	for _,child := range root.child {
		count += countXMLElements(child, elementName)
	}
	return count
}

func analyseSizeLayoutsXML(dir string, result *AnalysisResult) {
	resDirs := getAllLayoutDirs(dir)
	// map: baseDir -> layoutDirs
	m := make(map[string][]string)
	for _, resDir:= range resDirs {
		i := strings.LastIndex(resDir, "/")
		key := resDir[0:i]
		value := resDir[i + 1:]
		mList := m[key]
		m[key] = append(mList, value)
	}

	for baseDir, layoutDirs := range m {
		sort.Strings(layoutDirs)
		// list of maps: [](xmlFile -> xmlElements)
		var list []map[string]*XMLElement
		for _, layoutDir := range layoutDirs {
			totalDir := path.Join(baseDir, layoutDir)
			//(layoutDir -> (xmlFile -> xmlElements))
			xmlElementMap := make(map[string]*XMLElement)
			for _, xmlFile := range listXMLFiles(totalDir) {
				rootElement := parseExpandIncludes(totalDir, xmlFile)
				if rootElement != nil {
					xmlElementMap[xmlFile] = rootElement
				}
			}
			list = append(list, xmlElementMap)
		}
		if len(list) <= 1 {
			continue
		}
		layoutMap0 := list[0]
		layoutDir0 := layoutDirs[0]

		for i, layoutMap := range list {
			if i == 0 {
				continue
			}
			for xmlFile, xmlElement0 := range layoutMap0 {
				elementsTypes0 := NewXMLElementTypes()
				elementsTypes0.analyse(xmlElement0, path.Join(layoutDir0, xmlFile))
				if xmlElement1, ok := layoutMap[xmlFile]; ok {
					elementsTypes1 := NewXMLElementTypes()
					layoutDir1 := layoutDirs[i]
					elementsTypes1.analyse(xmlElement1, path.Join(layoutDir1, xmlFile))
					match := NewMultiLayoutMatchXML(baseDir, xmlFile, layoutDir0, xmlElement0, &elementsTypes0,
						layoutDir1, xmlElement1, &elementsTypes1)
					result.multiLayoutXML.matches = append(result.multiLayoutXML.matches, match)
				}
			}
		}
	}
}

func (this *RepoAnalyser) analyseRepo(repo FetchedRepo, out chan<- AnalysisResult) {
	println("Analyse: " + repo.repoItem.Html_url)

	var result = NewAnalysisResult(repo)

	result.libInDescription = strings.Contains(strings.ToLower(repo.repoItem.Description), "lib")
	result.isApp = isApp(&repo)
	mainLayoutDirs := getMainLayoutDirs(repo.dir)
	result.mainLayoutDirsXML = mainLayoutDirs
	for _, line:= range mainLayoutDirs {
		for _,xmlFile := range listXMLFiles(line) {
			xmlElement := parseExpandIncludes(line, xmlFile)
			if xmlElement == nil {
				println("cant' parse: " + path.Join(line, xmlFile))
				continue
			}
			result.mainLayoutElementTypes.analyse(xmlElement, path.Join(line, xmlFile))
			result.mainLayoutCount ++
		}
	}

	result.inCodeLayoutAnalysis.analyse(repo)
	analyseSizeLayoutsXML(repo.dir, &result)

	//analyseSizeLayoutsXML("../../../Lablet", &result)
	out <- result
}