package main
import (
	"encoding/xml"
	"io"
	"strings"
)

type XMLElement struct {
	name xml.Name
	attrs []xml.Attr
	child []*XMLElement
	parent *XMLElement
}

func (this *XMLElement)parse(in io.Reader) {
	decoder := xml.NewDecoder(in)

	var current *XMLElement

	for {
		token, _ := decoder.Token()
		if token == nil {
			break
		}
		switch xmlElement := token.(type) {
		case xml.StartElement:
			if current == nil {
				current = this
			} else {
				current = &XMLElement{parent: current}
			}
			current.name = xmlElement.Name;
			current.attrs = xmlElement.Attr;
		case xml.EndElement:
			if (current != this) {
				parentElement := current.parent;
				parentElement.child = append(parentElement.child, current)
				current = current.parent
			}
		}
	}
}

func (this *XMLElement)toString() string {
	out := ""
	out += this.name.Local + "\n"
	for _, child := range this.child {
		for parent := child.parent; parent != nil; parent = parent.parent {
			out += "\t"
		}
		out += child.toString()
	}
	return out
}

func testXMLElementParsing() {
	doc := `<LinearLayout>
				<Button id="id"/>
				<LinearLayout/>
				<LinearLayout>
					<Button id="id2"/>
				</LinearLayout>
			</LinearLayout>`

	element := XMLElement{}
	element.parse(strings.NewReader(doc))

	println(element.toString())
}