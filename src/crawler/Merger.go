package main

import (
	"os"
	"strconv"
	"path"
)


type Merger struct {
	summaryFile *os.File
	matchFile *os.File
	resultDir string
	counter int
	nProjectHasMatch int
}

func openFileForWrite(fileName string, nFails int) *os.File {
	f, err := os.OpenFile(fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0660)
	if err != nil {
		if nFails >= 3 {
			panic(err)
		}
		return openFileForWrite(fileName, nFails + 1)
	}
	return f
}

func openOutputFile(fileName string) *os.File {
	f := openFileForWrite(fileName, 0)
	f.Truncate(0)
	return f
}

func NewMerger(resultDir string, summaryFileName string) Merger {
	merger := Merger{}

	// clean up dir
	os.RemoveAll(resultDir)
	os.MkdirAll(resultDir, 0770)

	merger.summaryFile = openOutputFile(path.Join(resultDir, summaryFileName))
	writeSummaryHeader(merger.summaryFile, NewXMLElementTypes())

	merger.matchFile = openOutputFile(path.Join(resultDir, "matches.csv"))
	merger.matchFile.WriteString(multiLayoutMatchXMLCSHeaderString() + "\n")

	merger.resultDir = resultDir

	return merger
}

func (this *Merger)writeLine(fileName string, line string) {
	f := openFileForWrite(path.Join(this.resultDir, fileName), 0)
	defer f.Close()
	f.WriteString(line + "\n")
	f.Sync()
}

func (this *Merger) merge(resultChannel <-chan AnalysisResult) {
	for result := range resultChannel {
		this.mergeResult(&result)
	}

	println("nProjectHasMatch/nProjects: " + strconv.Itoa(this.nProjectHasMatch) + "/" + strconv.Itoa(this.counter))
	this.summaryFile.Close()
	this.matchFile.Close()
}

func btoa(value bool) string {
	if value {
		return "1"
	}
	return "0"
}

func abs(value1 int) int {
	if value1 >= 0 {
		return value1
	}
	return -value1;
}

func (this *Merger) writeMatches(result *AnalysisResult) {
	if len(result.multiLayoutXML.matches) == 0 {
		return
	}
	this.nProjectHasMatch ++

	for _, match := range result.multiLayoutXML.matches {
		this.matchFile.WriteString(match.toCSString() + "\n")
	}
}

func (this *Merger) writeMainLayoutDirsXML(result *AnalysisResult) {
	for _, dir := range result.mainLayoutDirsXML {
		this.writeLine("mainLayoutDirsXML.csv", dir)
	}
}

func writeSummaryHeader(f *os.File, types XMLElementTypes) {
	var out string
	out += analysisResultHeader(types)
	f.WriteString(out + "\n")
}

func (this *Merger) writeSummary(result *AnalysisResult) {
	var out string
	out += result.toCCString()
	this.summaryFile.WriteString(out + "\n")
}

func writeHeaderXMLTypeNames(prefix string, names []string) (out string) {
	for _, name := range names {
		out += prefix + name + ","
	}
	return out
}

func writeHeaderXMLTypes(types XMLElementTypes) (out string) {
	out += writeHeaderXMLTypeNames("nRootLayout_", layoutTypes)
	out += writeHeaderXMLTypeNames("nElement_", layoutTypes)
	out += writeHeaderXMLTypeNames("nElement_", elementTypes)
	return out
}

func writeXMLTypes(types XMLElementTypes) (out string) {
	out += writeXMLType(layoutTypes, types.rootLayoutCounts)
	out += writeXMLType(layoutTypes, types.layoutCounts)
	out += writeXMLType(elementTypes, types.elementCounts)
	return out
}

func writeXMLType(keys []string, typeCount map[string]int) (out string) {
	for _, key := range keys {
		value, _ := typeCount[key]
		out += strconv.Itoa(value) + ","
	}
	return out
}

func (this *Merger)writeUnknownElements(results *AnalysisResult) {
	for element, layoutFile := range results.mainLayoutElementTypes.unknownElements {
		this.writeLine("unkownElements.csv", element + "," + layoutFile)
	}
}

func (this *Merger) mergeResult(result *AnalysisResult) {
	this.writeSummary(result)
	this.writeMainLayoutDirsXML(result)
	this.writeMatches(result)
	this.writeUnknownElements(result)

	/*for _, unknownElement := range result.mainLayoutElementTypes.unknownElements {
		println(unknownElement)
	}*/
	this.counter ++;
	println("Merged items: " + strconv.Itoa(this.counter))
}

