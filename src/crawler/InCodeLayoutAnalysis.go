package main
import "strconv"


type InCodeLayoutAnalysis struct {
	nAbsoluteLayout int
	nFrameLayout int
	nLinearLayout int
	nTableLayout int
	nGridLayout int
	nRelativeLayout int
}

func (this *InCodeLayoutAnalysis) analyse(repo FetchedRepo) {
	this.nAbsoluteLayout = countLayoutType(repo.dir, "AbsoulteLayout")
	this.nFrameLayout = countLayoutType(repo.dir, "FrameLayout")
	this.nLinearLayout = countLayoutType(repo.dir, "LinearLayout")
	this.nTableLayout = countLayoutType(repo.dir, "TableLayout")
	this.nRelativeLayout = countLayoutType(repo.dir, "RelativeLayout")
}

func inCodeLayoutAnalysisHeader(prefix string) string {
	return prefix + "AbsoluteLayout," + prefix + "FrameLayout," + prefix + "LinearLayout," + prefix + "TableLayout," +
	prefix + "RelativeLayout"
}

func (this* InCodeLayoutAnalysis)toCSString() string {
	return strconv.Itoa(this.nAbsoluteLayout) + "," + strconv.Itoa(this.nFrameLayout) + "," +
		strconv.Itoa(this.nLinearLayout) + "," + strconv.Itoa(this.nTableLayout) + "," +
		strconv.Itoa(this.nRelativeLayout)
}

/*
Count layout instantiated with new.
 */
func countLayoutType(dir string, layoutType string) int {
	lines := bashCommandArray("find " + dir + " -name '*.java' -exec grep \"new\\s\\+" + layoutType +
		"\\s\\?(\" {} \\;")
	return len(lines)
}