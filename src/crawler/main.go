package main

import (
	"os/exec"
	"os/user"
	"path"
)


func bashCommandErr(command string) ([]byte, error) {
	cmd := exec.Command("bash", "-c", command)
	return cmd.CombinedOutput()
}

func bashCommand(command string) []byte {
	out, err := bashCommandErr(command)
	if (err != nil) {
		//println("bash error:" + command)
		return make([]byte, 0)
		//panic(err)
	}
	return out
}

func main() {
	usr, _ := user.Current()
	outputDir := path.Join(usr.HomeDir, "crawlerOut");
	//urlFetcher := UrlFetcher{outDir: outputDir, nItems: 40, itemsPerPage: 100, useCachedUrls: true,
	//	searchTerm : "android+app", channelSize: 5}
	//urlFetcher := FDroidUrlFetcher{nItems: 10000, outDir: outputDir, channelSize: 5, debugRepoName: "Spark_360"}
	urlFetcher := FDroidUrlFetcher{nItems: 10000, outDir: outputDir, channelSize: 5}
	repoFetcher := RepoFetcher{pullExisting: false, baseDir: outputDir, channelSize: 5}
	repoAnalyser := RepoAnalyser{channelSize: 5}
	merger := NewMerger(path.Join(outputDir, "results"), "summary.csv")

	merger.merge(repoAnalyser.goAnalyse(repoFetcher.goFetch(urlFetcher.goFetch())))
}
