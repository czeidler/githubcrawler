package main
import (
	"os"
	"bufio"
	"strings"
	"path"
)


type FDroidUrlFetcher struct {
	nItems int
	outDir string
	channelSize int
	debugRepoName string
}

func (this *FDroidUrlFetcher) goFetch() chan RepoItem {
	var urlFetchedChannel = make(chan RepoItem, this.channelSize)
	go this.fetch(urlFetchedChannel)
	return urlFetchedChannel
}

func ensureFDroidRepo(repoDir string) {
	if _, err := os.Stat(repoDir); os.IsNotExist(err) {
		os.MkdirAll(repoDir, 0770)
		println("clone fdroiddata")
		bashCommand("git clone --depth=1 https://gitlab.com/fdroid/fdroiddata.git " + repoDir)
	}
}

func (this *FDroidUrlFetcher) fetch(out chan<- RepoItem) {
	defer close(out)

	fDroidRepoDir := path.Join(this.outDir, "fdroiddata")
	fDroidMetaDataDir := path.Join(fDroidRepoDir, "metadata");
	ensureFDroidRepo(fDroidRepoDir);

	files := bashCommandArray("ls -1 " + fDroidMetaDataDir + "/*.txt")
	var counter int
	for _, file := range files {
		if counter > this.nItems {
			break
		}

		var repoItem RepoItem

		f, _ := os.Open(file)
		defer f.Close()

		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			line := scanner.Text()
			replacer := strings.NewReplacer(" ", "_", "(", "", ")", "", "/", "_")
			if strings.HasPrefix(line, "Name:") {
				repoItem.Name = replacer.Replace(line[len("Name:"):])
				continue
			}
			if strings.HasPrefix(line, "Auto Name:") && repoItem.Name == "" {
				repoItem.Name = replacer.Replace(line[len("Auto Name:"):])
				continue
			}
			if strings.HasPrefix(line, "Summary") {
				repoItem.Description = line[len("Summary:"):]
				continue
			}
			if strings.HasPrefix(line, "Repo:") {
				repoItem.Html_url = line[len("Repo:"):]
				continue
			}
		}
		if this.debugRepoName != "" && this.debugRepoName != repoItem.Name {
			continue
		}
		out <- repoItem
		counter++
	}
}